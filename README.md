![Alt text](https://bitbucket.org/tonyperezjr/poketest.front/raw/1d145547ae2c1a6d235315f8606f19d7aded6b83/src/assets/img/logo.png)

[![npm version](https://badge.fury.io/js/@angular%2Fcore.svg)](https://badge.fury.io/js/@angular%2Fcore)

![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![Angular](https://img.shields.io/badge/angular-%23DD0031.svg?style=for-the-badge&logo=angular&logoColor=white)
![RxJS](https://img.shields.io/badge/rxjs-%23B7178C.svg?style=for-the-badge&logo=reactivex&logoColor=white)
![SASS](https://img.shields.io/badge/SASS-hotpink.svg?style=for-the-badge&logo=SASS&logoColor=white)
![Adobe Illustrator](https://img.shields.io/badge/adobeillustrator-%23FF9A00.svg?style=for-the-badge&logo=adobeillustrator&logoColor=white)
![Bootstrap](https://img.shields.io/badge/bootstrap-%23563D7C.svg?style=for-the-badge&logo=bootstrap&logoColor=white)

# Poketest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.5.

## Get started

## 👯‍♀️ Clone the repo

`git clone URL-REPO.git`

## 🗺️ Install npm packages

Install the `npm` packages described in the `package.json` and verify that it works: `npm install`

## 🚀 Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Shut it down manually with `Ctrl-C`.

## 📋 Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## 📦 Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## 📌 Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run unit testing `npm run test -- --include src/app/pages/dashboard/dashboard.component.spec.ts`

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
