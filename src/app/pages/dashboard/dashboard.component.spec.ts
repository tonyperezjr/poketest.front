import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DashboardComponent } from './dashboard.component';
import { PokeService } from 'src/app/service/poke.service';
import { ReactiveFormsModule } from '@angular/forms';
import { PokeServiceMock } from 'src/app/mocks/services/poke-service.mock';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let pokeService: PokeService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      providers: [ 
        {
          provide: PokeService,
          useClass: PokeServiceMock
        }
      ],
      imports: [
        HttpClientTestingModule, 
        HttpClientModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    pokeService = TestBed.inject(PokeService);
    fixture.detectChanges();
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should have call getListPokes function', () => {
    const pokeServiceSpy = spyOn(pokeService, 'getList').and.callThrough();
    
    component.getListPokes();

    expect(pokeServiceSpy).toHaveBeenCalledOnceWith(0,12);
  });
});
