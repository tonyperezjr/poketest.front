import { Component, OnInit, Injectable, Input, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

import { pokeList } from 'src/app/model/poke-list.model';
import { PokeService } from '../../service/poke.service'
import { Pokemon } from '../../model/pokemon.model'
import { LoadService } from '../../service/load.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  
  @Output() pokemonCurrent: Pokemon | undefined;
  @Output() contolMessage: boolean = false;
  @Output() message: string | undefined;

  public init = 0;
  public end = 9;
  public nextPage = '';
  public subscription = new Subscription();
  public pokemonList: Pokemon[] = [];
  public form: FormGroup;

  constructor(private _pokeService: PokeService,
              private readonly fb: FormBuilder,
              public loadService: LoadService) { 
                this.form = this.fb.group({
                  pokename: ['', Validators.required]
                });
                this.getListPokes();                
  }

  ngOnInit(): void {  }

  public getListPokes(){
    this._pokeService.getList(this.init, this.end).subscribe((_pokeList: pokeList) => {
      this.loadPokes(_pokeList);           
    });
  }

  public loadPokes(_pokeList: pokeList){
    _pokeList.results.forEach(element => { 
      let splitPoke = element.url.replace("http://","").split("/");    
      this._pokeService.getPokemonById(splitPoke[6]).subscribe((_pokemon: Pokemon) =>{
        if(this.pokemonList.length <= 1) this.setPoke(_pokemon.id); 
        this.pokemonList.push({ ... _pokemon });         
      })      
    });
  }

  public searchPoke(_namePoke: string){
    console.log(this.form.valid);
    
    _namePoke = _namePoke.toLowerCase();
    this._pokeService.getPokemonByName(_namePoke).subscribe(
      (_poke:Pokemon) => {
        if(_poke.id != undefined){
          this.pokemonList = [];
          // show unique poke
          // this.pokemonList.push(_poke);
          this.message = ``;
          this.contolMessage = false;
          this.setPoke(this.pokemonList[0].id)
        }else{
          this.message = `Não encontramos seu Pokemon "${_namePoke}".`;
          this.contolMessage = true;  
          this.pokemonList = [];
          this.getListPokes();
        }
      },
      (err:HttpErrorResponse) => {
        this.message = `Não encontramos o Pokemon "${_namePoke}"`;
        this.contolMessage = true;          
        console.log(err);        
      },
      () => {  }
    );
  }

  public setPoke(_id: number){ 
    this.pokemonList.forEach(element => {
      if(element.id == _id){ this.pokemonCurrent = element; }      
    });      
    window.scroll(0,0);
  }

  submit() {    
    this.searchPoke(this.form.get('pokename')?.value)    
  }

  public loadMore() {

    if(this.pokemonList.length != 0) this.init += this.end;
    
    this._pokeService.getList(this.init, this.end).subscribe((_pokeList: pokeList) => {
      this.loadPokes(_pokeList);
    });
  }

}

