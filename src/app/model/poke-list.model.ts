export interface pokeList {
    results: [
        { 
            name: string, 
            url: string,
            count: number,
            previous: null | string,
            next:  null | string
        }
    ]
}  