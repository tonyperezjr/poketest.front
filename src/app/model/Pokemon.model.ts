export interface Pokemon {
    id: number,
    name: string, 
    url: string,
    sprites :{
        front_default: string
        other:{
            dream_world:{
                front_default: string
            }
        }
    }
    types: [
        {
            slot: number,
            type: {
                name: string,
                url?: string
            }
        }
    ],
    stats: [
        {
        base_stat: number,
        effort: number,
        stat: { name: string }
        }
    ]
}