import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadService {

  public loadingStatus: boolean = true;
  public control: number = 0;

  get loading(): boolean {
    return this.loadingStatus;
  }

  public initLoading(): void {
    this.control++;
    this.loadingStatus = true;
  }
  
  public finishLoading(): void {
    this.control--;
      this.loadingStatus = this.control <= 0 ? false : true;
  }

}
