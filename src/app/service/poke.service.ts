import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { pokeList } from '../model/poke-list.model'
import { Pokemon } from '../model/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokeService {
  basehref: string = environment.baseApi;

  requestOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
    }),
    method: 'GET',
    mode: 'no-cors'
  };

  constructor(public http: HttpClient) { }

  public getList(_offset: number, _limit: number): Observable<pokeList>{
      return this.http.get<pokeList>(`${this.basehref}pokemon/`, {
        params: { 
                  'offset': _offset.toString(), 
                  'limit': _limit.toString()
                },
                ... this.requestOptions
              });
  }

  public getPokemonById(_id: string){
    return this.http.get<Pokemon>(`${this.basehref}pokemon/${_id}`, this.requestOptions);
  }

  
  public getPokemonByName(_name: string){
    return this.http.get<Pokemon>(`${this.basehref}pokemon/${_name}`, this.requestOptions);
  }
}
