import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, finalize } from 'rxjs/operators';

import { LoadService } from '../service/load.service';

@Injectable()
export class LoadInterceptor implements HttpInterceptor {

  constructor(public loadService: LoadService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loadService.initLoading();   
    
    return next.handle(request).pipe(delay(2000), finalize(() => {
      this.loadService.finishLoading();
    }));
  }
}

export const LoadingInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: LoadInterceptor,
  multi: true
};

