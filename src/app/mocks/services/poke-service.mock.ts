import { Observable, of } from "rxjs"
import { pokeList } from '../../model/poke-list.model';

export class PokeServiceMock{
    getList(_offset: number, _limit: number): Observable<any>{
        return of(
            {
                results: [
                    { 
                        count: 1118,
                        next: "https://pokeapi.co/api/v2/pokemon/?offset=12&limit=12",
                        previous: 'null',
                        name: 'bulbasaur', 
                        url: 'https://pokeapi.co/api/v2/pokemon/1/' 
                    },
                    { 
                        count: 2222,
                        next: "https://pokeapi.co/api/v2/pokemon/?offset=24&limit=12",
                        previous: "https://pokeapi.co/api/v2/pokemon/?offset=12&limit=12",
                        name: 'ivysaur', 
                        url: 'https://pokeapi.co/api/v2/pokemon/2/' 
                    }
                ]
            }  
        )
    }
}

