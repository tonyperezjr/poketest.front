import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavheaderComponent } from './navheader/navheader.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    NavheaderComponent,
    PageNotFoundComponent
  ],
  imports: [CommonModule],
  exports :[NavheaderComponent],
  providers: [],
})
export class SharedModule { }
