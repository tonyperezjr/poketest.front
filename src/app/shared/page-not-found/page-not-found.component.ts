import { Component, OnInit } from '@angular/core';
import { LoadService } from 'src/app/service/load.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  constructor(public loadService: LoadService) {
    this.loadService.loadingStatus = false;
   }

  ngOnInit(): void { }

}
