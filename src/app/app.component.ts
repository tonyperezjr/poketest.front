import { Component } from '@angular/core';
import { LoadService } from './service/load.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'poketest';

  constructor(public loadService: LoadService) {   }
  
}
